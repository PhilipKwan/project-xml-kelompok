﻿using Exam_1_WPF.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_1_WPF.Validator
{
    class UserBiodataValidator : AbstractValidator<UserBiodata>
    {
        public UserBiodataValidator()
        {
            //ID
            RuleFor(Q => Q.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty")
                .Must(UniqueId).WithMessage("{PropertyName} is not unique");

            //NAME
            RuleFor(Q => Q.Name)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty")
                .MinimumLength(5).WithMessage("Length ({TotalLength}) of {PropertyName} invalid")
                .Must(BeAValidName).WithMessage("{PropertyName} Contain Invalid char only")
                .Must(Containt2word).WithMessage("{PropertyName} Contain 2 word or more");

            //EMAIL -> REGEX
            RuleFor(Q => Q.Email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty")
                .EmailAddress().WithMessage("{PropertyName} is not a valid email")
                .Must(UniqueEmail).WithMessage("{PropertyName} must use accelist email");

            //GENDER
            RuleFor(Q => Q.Gender)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("{PropertyName} is Empty");
        }

        private bool UniqueEmail(string email)
        {
            return MainWindow.ListUsers.Any(Q => Q.Email == email) == false;
        }

        private bool Containt2word(string name)
        {
            return name.Contains(" ");
        }

        protected bool UniqueId(int number)
        {
            return MainWindow.ListUsers.Any(Q => Q.Id == number) == false;
        }

        protected bool BeAValidName(string name)
        {
            name = name.Replace(" ", "");
            name = name.Replace("-", "");
            return name.All(Char.IsLetter);
        }
    }
}
