﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_1_WPF.Models
{
    public class UserBiodata
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Gender { set; get; }
        public string Status { set; get; }

    }
}
