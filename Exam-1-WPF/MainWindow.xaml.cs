﻿using Exam_1_WPF.Models;
using Exam_1_WPF.Validator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam_1_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<UserBiodata> ListUsers { set; get; }

        public MainWindow()
        {
            InitializeComponent();
            Title = "HUTAMA-PHILIP";

            var hobbyList = new List<string> { "MALE", "FEMALE" };
            UserGender.ItemsSource = hobbyList;

            //BERSIIN
            ListUsers = new List<UserBiodata>();
            ListUsers.Add(new UserBiodata
            {
                Id = 1,
                Name = "Pilip kwan",
                Email = "pilip@gmail.com",
                Gender = "Male",
                Status = "Active",
            }) ;

            UserDG.ItemsSource = ListUsers;
        }

        private void UserAdd_Click(object sender, RoutedEventArgs e)
        {
            var user = new UserBiodata();

            //INPUT
            try
            {
                user.Id = Int32.Parse(UserID.Text);
            }
            catch (Exception error) { }

            user.Name = UserName.Text;
            user.Email = UserEmail.Text;
            user.Gender = UserGender.SelectedItem?.ToString();
            user.Status = "Active";

            //VALIDATOR
            var validator = new UserBiodataValidator();
            var result = validator.Validate(user);

            if (result.IsValid == false)
            {
                var messages = result.ToString("\n");
                ErrorListBox.ItemsSource = result.Errors.ToList();
                return;
            }

            else
            {
                MessageBox.Show("Success Add Employee!");
            }

            ListUsers.Add(user);
            var checkUser = ListUsers;
            UserDG.Items.Refresh();
        }

        private void clearBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        public void ClearForm()
        {
            //CLEAR FORM DATA
            UserID.Text = null;
            UserName.Text = null;
            UserEmail.Text = null;
            UserGender.SelectedItem = null;
        }

        
    }
}
